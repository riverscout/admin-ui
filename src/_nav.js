export default {
  items: [

    {
      title: true,
      name: 'System'
    },

    {
      name: 'Home',
      url: '/pages/homepage',
      icon: 'icon-home',
    },
    {
      name: 'Devices',
      url: '/pages/devices',
      icon: 'cui-laptop',
    },
    {
      name: 'Groups',
      url: '/pages/groups',
      icon: 'icon-layers',

    }
  
    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: 'Account'
    // },
 

    // {
    //   name: 'Logout',
    //   url: '/pages/login',
    //   icon: 'cui-account-logout',
    //   color : 'red',
    //   //variant: 'danger',
    //   variant:"outline-danger"
    //   //attributes: { target: '_blank', rel: 'noopener' } if you want to open a new tab, use this
    // },
  ]
}
