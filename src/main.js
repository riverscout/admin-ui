// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
// import the Google Maps plugin
// see tutorial here: https://alligator.io/vuejs/vue-google-maps/
import * as VueGoogleMaps from "vue2-google-maps";


// todo
// cssVars()

Vue.use(BootstrapVue)

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyC6CaWXttXzsjX5OfTO7ddHnTwnGsI0tnQ",
    libraries: "places" // necessary for places input
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App,
    VueGoogleMaps
  }
})
