import Vue from 'vue'
import Router from 'vue-router'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const Homepage = () => import('@/views/pages/Homepage')
const Devices = () => import('@/views/pages/Devices')
const Groups = () => import('@/views/pages/Groups')





// Views - Pages
const Page404 = () => import('@/views/other/Page404')
const Page500 = () => import('@/views/other/Page500')
const Login = () => import('@/views/other/Login')
const Register = () => import('@/views/other/Register')


Vue.use(Router)

// configure the breadcrumbs on various pages
export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/pages/homepage',
      name: 'Home',
      // specify the main container
      component: DefaultContainer,
      // this is the component that will change with different routes
      children: [
        {
          path: 'pages/homepage',
          name: 'Homepage',
          component: Homepage
        }
      ]
    },
    {
      path: '/other',
      redirect: '/other/404',
      name: 'Other',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        }
      ]
    },
    {
      path: '/pages/devices',
      redirect: '/pages/devices',
      name: 'Devices',
      component: DefaultContainer,
      children: [
        {
          path: '/pages/devices',
          name: 'Devices',
          component: Devices
        }
      ]
    },
    {
      path: '/pages/groups',
      redirect: '/pages/groups',
      name: 'Devices',
      component: DefaultContainer,
      children: [
        {
          path: '/pages/groups',
          name: 'Groups',
          component: Groups
        }
      ]
    }
  ]
})
